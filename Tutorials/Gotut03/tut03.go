// https://golang.org/ search by Go tutotial video
// 29/4/2018
// You can edit this code!
// Click here and start typing.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/api/psi", neapsi) // - format date/time

	//	http.HandleFunc("/chair", func(w http.ResponseWriter, r *http.Request) {
	//		fmt.Fprintln(w, "chair")
	//	})
	//	http.HandleFunc("/lpk1", barhandler) // - print text via Func

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func neapsi(w http.ResponseWriter, r *http.Request) {
	type PsiStruct struct {
		RegionMetadata []struct {
			Name          string `json:"name"`
			LabelLocation struct {
				Latitude  float64 `json:"latitude"`
				Longitude float64 `json:"longitude"`
			} `json:"label_location"`
		} `json:"region_metadata"`
		Items []struct {
			Timestamp       time.Time `json:"timestamp"`
			UpdateTimestamp time.Time `json:"update_timestamp"`
			Readings        struct {
				O3SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"o3_sub_index"`
				Pm10TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm10_twenty_four_hourly"`
				Pm10SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm10_sub_index"`
				CoSubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"co_sub_index"`
				Pm25TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm25_twenty_four_hourly"`
				So2SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"so2_sub_index"`
				CoEightHourMax struct {
					West     float64 `json:"west"`
					National float64 `json:"national"`
					East     float64 `json:"east"`
					Central  float64 `json:"central"`
					South    float64 `json:"south"`
					North    float64 `json:"north"`
				} `json:"co_eight_hour_max"`
				No2OneHourMax struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"no2_one_hour_max"`
				So2TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"so2_twenty_four_hourly"`
				Pm25SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm25_sub_index"`
				PsiTwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"psi_twenty_four_hourly"`
				O3EightHourMax struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"o3_eight_hour_max"`
			} `json:"readings"`
		} `json:"items"`
		APIInfo struct {
			Status string `json:"status"`
		} `json:"api_info"`
	}

	resp, _ := http.Get("https://api.data.gov.sg/v1//environment/psi")
	bytes, _ := ioutil.ReadAll(resp.Body)
	stringbody := string(bytes)
	fmt.Fprintln(w, stringbody)
	resp.Body.Close()

	//var psi PsiStruct
	//json.Unmarshal(resp, &psi) // is this okay unmarshal the "resp" which contains PSI
	// content into 'psiStruct' which we defined above

	// how to pick up an item of Struct ....30/4 10:12am

}

func barhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "this is LPK1 with time.Now()")
	t := time.Now()

	//	fmt.Fprintln(w, time.Now())
	fmt.Fprintln(w, t)
	// https://stackoverflow.com/questions/20234104/how-to-format-current-time-using-a-yyyymmddhhmmss-format
	const layout = "Monday, January 2, 2006 at 3:04pm (MST)"
	fmt.Fprintln(w, t.Format(layout)) // must be 2006?
}

func wstpost() {
	// https://www.youtube.com/watch?v=ccANcNk8Dac - news aggregator tutorial 10
	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	stringbody := string(bytes)
	fmt.Println(stringbody)
	resp.Body.Close()
}
