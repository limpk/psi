package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/lpk1", barhandler) // - print text via Func
	http.HandleFunc("/lpk2", formattime) // - format date/time

	http.HandleFunc("/chair", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "chair")
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
