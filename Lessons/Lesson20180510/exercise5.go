package main

// 2 questions
// (1) how to call a function, ("/api/psi", psiAPIHandler), so that /api/now does not need to repeat /psi codes
// (2) how to insert a new line before Json printing, or can I insert new line in template printing?

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const respTmpl = `PSI 24H - National: {{ .PSI24.National }}, North: {{ .PSI24.North }}, East: {{ .PSI24.East }}, South: {{ .PSI24.South }}, West: {{ .PSI24.West }}
(1) PM10 National is {{ .PM10.National }}
(2) PM10 North is {{ .PM10.North }}
(3) PM10 East is {{ .PM10.East }}
(4) PM10 South is {{ .PM10.South }}
(5) PM10 West is {{ .PM10.West }}
(6) PSI National is {{ .PSI24.National }}
(7) PSI North is {{ .PSI24.North }}
(8) PSI East is {{ .PSI24.East }}
(9) PSI South is {{ .PSI24.South }}
(10) PSI West is {{ .PSI24.West }}`

func main() {
	http.HandleFunc("/api/psi/json", psiAPIJSONHandler)
	http.HandleFunc("/api/psi/text", psiAPITextHandler)
	http.HandleFunc("/api/now", nowAPIHandler) // Exercise#05 9/5/2018
	http.HandleFunc("/formatTime", formatTime) // Exercise#05 9/5/2018
	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func psiAPIJSONHandler(w http.ResponseWriter, r *http.Request) {
	resp, err := http.Get("https://api.data.gov.sg/v1//environment/psi")
	if err != nil {
		panic(err)
	}

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	parsedResp := PSIApiResp{}
	err = json.Unmarshal(respBytes, &parsedResp)
	if err != nil {
		panic(err)
	}

	psi24 := parsedResp.Items[0].Readings.PsiTwentyFourHourly

	// Exercise 05 below
	psi := PSI24{
		North: psi24.North,
		South: psi24.South,
		East:  psi24.East,
		West:  psi24.West,
	}
	mapJSON, err := json.Marshal(psi)
	if err != nil {
		panic(err)
	}
	w.Write(mapJSON)
	// Exercise 05 above
}

type PSI24 struct {
	North int `json:"north"`
	South int `json:"south"`
	East  int `json:"east"`
	West  int `json:"west"`
}

func psiAPITextHandler(w http.ResponseWriter, r *http.Request) { // exercise 05
	err := func() error {
		resp, err := http.Get("https://api.data.gov.sg/v1//environment/psi")
		if err != nil {
			return err
		}

		respBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		defer resp.Body.Close()

		parsedResp := PSIApiResp{}
		err = json.Unmarshal(respBytes, &parsedResp)
		if err != nil {
			return err
		}

		psi24 := parsedResp.Items[0].Readings.PsiTwentyFourHourly
		pm10 := parsedResp.Items[0].Readings.Pm10TwentyFourHourly

		tmpl, err := template.New("respTmpl").Parse(respTmpl)
		if err != nil {
			return err
		}

		err = tmpl.Execute(w, map[string]interface{}{
			"PSI24": psi24,
			"PM10":  pm10,
		})

		if err != nil {
			return err
		}

		return nil
	}()

	if err != nil {
		fmt.Println("erv:", err)
		// more things happen here
	}
}

func nowAPIHandler(w http.ResponseWriter, r *http.Request) {
	p := fmt.Fprintln
	t := time.Now()
	p(w, t.Format("2006-01-02 15:04:05\n"))
}

func formatTime(w http.ResponseWriter, r *http.Request) {
	// p := fmt.Println
	p := fmt.Fprintln
	t := time.Now()
	p(w, "(1) ", t.Format(time.RFC3339))

	t1, e := time.Parse(
		time.RFC3339,
		"2012-11-01 22:08:41+00:00") // output 0001-01-01 00:00:00 +0000 UTC??
	p(w, "(2) ", t1)

	p(w, "(3) ", t.Format("3:04PM"))
	p(w, "(4) ", t.Format("Mon Jan _2 15:04:05 2006")) // must it be 2006?
	// Output (4)  Thu May  3 12:30:30 2018 when year is 2006
	// Output (4)  Thu May  3 12:32:18 3007 when year is 2007
	p(w, "(5) ", t.Format("2006-01-02 15:04:05.999999-07:00"))
	form := "3 04 PM"
	t2, e := time.Parse(form, "8 41 PM")
	p(w, "(6) ", t2)

	fmt.Fprintf(w, "(7) %d-%02d-%02d T %02d:%02d:%02d-00:00\n",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	ansic := "Mon Jan _2 15:04:05 2006"
	_, e = time.Parse(ansic, "8:41PM")
	p(w, "(8) ", e)
}
