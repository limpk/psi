package main

// add Delete latest entry
// add ClearAll
//error  Questions
// 1/ Why Add(), why not add()? or it is a naming convention?
// 2/ Is it the right way to use the delCheck field to check certain status?

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"gitlab.com/shasderias/history"
)

// MaxHistory is the number of history events tracked. To address "green" error after saving
const (
	LuckyMaxHistory = 8 // testing purpose
)

// HistoryResp data structure -> History -> HistoryEntries
type HistoryResp struct {
	CurLuckyNum     int             `json:"current_lucky_num"`
	LuckyNumHistory []history.Entry `json:"lucky_num_history"`
}

func main() {
	luckyHistory, err := history.New(LuckyMaxHistory)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/api/lucky", newLuckyAPIHandler(luckyHistory))
	http.HandleFunc("/api/lucky/removeOldest", del1HistoryHandler(luckyHistory))
	http.HandleFunc("/api/lucky/clear", clrallHistoryHandler(luckyHistory))

	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func newLuckyAPIHandler(h *history.History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		curLuckyNum := rand.Intn(100)

		resp := HistoryResp{
			CurLuckyNum:     curLuckyNum,
			LuckyNumHistory: h.Entries,
		}

		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}
		w.Write(respBytes)

		h.Add(curLuckyNum)
	}
}

//++ start of Exercie7.go

type error interface {
	Error() string
}

func del1HistoryHandler(h *history.History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := h.RemoveOldestEntry(); err != nil {
			fmt.Fprintln(w, err)
			return
		}
		fmt.Fprintln(w, "entry deleted")
		fmt.Println(h.Entries)
	}
}

func clrallHistoryHandler(h *history.History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := h.ClearAll(); err != nil {
			fmt.Fprintln(w, err)
			return
		}
		fmt.Fprintln(w, "history cleared")
		fmt.Println(h.Entries)
	}
}

//++ end of Exercie7.go
